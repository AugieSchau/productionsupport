#!/bin/bash

# sort by size
ls -lS /usr/bin | sed -e '/^d/d' -e '/^l/d' -e 's/^. 
\{10\} *[0-9]\{1,\} *\w\{1,\} *\w\{1,\} *\([0-9]\{1,\}\) *\w\{3\} *[0-9]\{1,2\ 
} *[0-9:]\{4,5\} *\(.\{1,\}\)/\2 \1/'

# sort by name
ls -l /usr/bin | sed -e '/^d/d' -e '/^l/d' -e 's/^.\
{10\} *[0-9]\{1,\} *\w\{1,\} *\w\{1,\} *\([0-9]\{1,\}\) *\w\{3\} *[0-9]\{1,2\} 
 *[0-9:]\{4,5\} *\(.\{1,\}\)/\2 \1/' | sort


# sort by size

ls -lS /usr/bin | awk ' { print $9, $5 }'

# sort by name
ls -lS /usr/bin | awk '{ print $9,$5 }' | sort