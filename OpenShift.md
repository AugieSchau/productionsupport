# OpenShift

OpenShift is an open source environment to host docker containers in packages call pods.  It can take a combination of containers that need to interact with each other and host them inside of a project.

Once a docker-compose configuration has been create a conversion tool called `kompose` can be used to generate the configuration files needed for building the project in OpenShift.

The `kompose convert` command will read a docker-compose.yaml file and generate additional yaml files to manage the building of the composed application in Kubernetes by default, but can have the `--provider` option set to OpenShift.  It would be a good idea to create these files in a separate directory.

    mkdir openshift
    cd openshift
    kompose convert --provider OpenShift -f ../docker-compose.yaml

Examine the generated files.  There are some edits that may be necessary, such as what the name of the project shoudl be, where to get the images from and what the route is for the application on the target server.  In the reference project there are some sample scripts that help with this.  It would be worth studying them for ideas on how to proceed.  

To use these files you will need a project on an OpenShift server to apply the configuration to.  

    oc login -u admin -padmin --insecure-skip-tls-verify=true dev1.conygre.com:8443 
    oc new-project sgtrades
    # send a comma separated list of the open-shift yaml files to oc apply
    oc apply -f `echo * | sed 's/ /,/g'`
