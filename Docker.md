# Docker Demonstration

Docker is the technology and a company that provides tools for creating useable and composable containers.  These containers can be described as "atomic packages"; everything that is necessary for the component to function is wrapped up inside of the containers.

To describe the container we create an image.  The image defines what the container will be made of any any steps necessary to build the container and make it ready for use.

There are many predefined image definitions available from places such as docker hub.  To search for a useful image try the docker search command.

    docker search mysql

To create a docker container use the docker run command.  Check out `docker run --help` for available options when running a container.  In particular look at the `-i`, `-t` and `-d` options.  It is also a good idea to assign your own name to the docker container with `--name` option.  The `-e` option allows commands to be run inside the container during startup.

    docker run -d --name db -e MYSQL_ROOT_PASSWORD=root mysql

To see which containers are currently running you can use the `docker container ls` command or the alias `docker ps`.  Add the `-a` option to see containers that have exited as well.

    docker ps -a

If a container is running and you would like to execute a command inside of the container, use the `docker exec` command.  For example to execute a bash shell inside of the mysql container that you named db, and to be able to interact with the shell try the following.

    docker exec -it db bash
    # Now you can interactively call into mysql which is running 
    # in the container.  Find the databases that are available...
    mysql -h localhost -u root -proot 
    mysql> show databases;

Once you are done with the container you will need to remove it.  Even if it has exited it is still around so that we can check its log files and other statistics.  Use the `docker rm` command to remove a container by name or by id.  Use the `-f` option to force a running container to be removed.

    docker rm -f db

When the container was run the docker daemon checked to see if the image requested was already installed locally.  If not then it pulled the image from the specified, or from the default, registry.  This image is installed locally so that the next time the container is run it will start up faster.  To see the locally installed images use the `docker images` command.  The `-q` option will provide just the id of the installed images;

    docker images

Images can be removed as well using the `docker rmi` command.  The `-f` option will skip the prompt to confirm that you wish to remove the image(s).  Again the image name or id can be used to identify which image(s) to remove.

    docker rmi -f mysql

## Custom docker images

Custom docker images can be build based on existing images.  The way of doing this is to define a Dockerfile that gives the instructions that the new image should package up and have ready for creating a container.

    mkdir MyCustomImage
    cd MyCustomImage

Use your favorite editor to create a new file.  Name it Dockerfile without any extension.  Inside of Dockerfile define what the starting image is and any modifications that are desired.  Checkout the documentation at [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/).

This example creates a custom mysql image that populates a sample database with test data.  It copies all of the scripts from a child directory called scripts into the director in the container that then runs them, in alphabetical order, on startup.

    FROM mysql:latest
    EXPOSE 3306
    ENV MYSQL_ROOT_PASSWORD=my-secret-pw
    COPY ./scripts/ /docker-entrypoint-initdb.d/

Save the Dockerfile and create a sub-directory named scripts.  Put your sql or sh scripts in the new sub-directory.

    mkdir scripts
    cd scripts

Create a script named script01_create_database.sql

    CREATE DATABASE sampledb;

    USE sampledb;

    CREATE TABLE trainers (
        id INT AUTO_INCREMENT,
        firstname VARCHAR(50) NOT NULL,
        lastname VARCHAR(60),
        start_date DATE,
        PRIMARY KEY (id)
    );

Create a second script named script99_populate_test_data.sql.  This script can later be removed when this image is being build for production, or left in, but setup not to add any new data.

    USE sampledb;

    INSERT INTO trainers (firstname, lastname, start_date)
    VALUES
    ('Augie', 'Schau', now()),
    ('Andy', 'Olsen', now()),
    ('Nick', 'Todd', now());

Change directories back to the directory that contains Docker file.  Use the `docker build` command to create a new image using the instructions provided in Dockerfile.  The `-t` option provides the new image name and tag and the `.` says that the starting context is the current directory.

    docker build -t trainerdb:1.0.0 .

There should now be a new image installed locally named trainerdb with a tag of 1.0.0.  We can run it to create a container.  Notice we no longer need to use `-e` to set the root password, and if we check the contents of the database it should have three rows in a table called trainers.

    docker run -d --name tdb -p3306:3306 trainerdb:1.0.0

You may execute a bash shell inside the container to confirm that the new image has created a populated the test database.

    docker exec -it tdb bash
    # mysql -uroot -pmy-secret-pw sampledb
    mysql> select * from trainers;

## Composing multile containers with docker-compose

Rename the first Dockerfile to dockerfile-sql and remove the ENV entry.

Create a second custom image that can interact with the first.  This example will use apache to expose a web page with the data displayed from the custom trainerdb container.  Name this Dockerfile dockerfile-app.

    FROM httpd
    RUN apt-get -y update
    RUN apt-get -y install mycli
    RUN mkdir /app
    COPY ./app/ /app/
    RUN chmod +x /app/startserver.sh
    ENTRYPOINT ["/app/startserver.sh"]

Create any application code in a sub-directory named app.  The Dockerfile is looking for a script named startserver.sh.

    #!/bin/bash

    # Wait for the database container to spin up and the database to be available.
    until mycli -h mysql -uroot -p"$MYSQL_ROOT_PASSWORD" sampledb -e "show tables;" | grep trainers
    do
        sleep 5s
    done

    # Build index.html
    htmlfilepath="/usr/local/apache2/htdocs/index.html"

    echo '<!DOCTYPE html><html><body><table><th><td>First Name</td><td>Last Name</td><td>Start Date</td></th>' > $htmlfilepath

    mycli -h mysql -u root -p"$MYSQL_ROOT_PASSWORD" sample -e "SELECT * FROM trainers;" --csv | awk -F ',' '{print "<tr><td>"$2"</td>","<td>"$3"</td>","<td>"$4"</td></tr>"}' >> $htmlfilepath

    echo '</table></body></html>' >> $htmlfilepath

    # Start the web server
    httpd-foreground

Create a docker-compose file.  The file should be named docker-compose with a .yml or .yaml extension.

    version: '3.0'
    services:
      mysql:
        container_name: mysql
        build:
          context: .
          dockerfile: dockerfile-sql
        image: demo/mydb:1.0.0
        environment:
          MYSQL_ROOT_PASSWORD: my-secret-pw
        ports:
          - "3306:3306"
        restart: always
      app:
        container_name: myapp
        build:
          context: .
          dockerfile: dockerfile-app
        image: demo/myapp:1.0.0
        environment:
          MYSQL_ROOT_PASSWORD: my-secret-pw
        ports:
          - "8080:80"
        links:
          - mysql:mysql

Use `docker-compose up` to create the images and create instance of the containers based on the new images.  Make sure that you are in the directory that contains the docker-compose.yaml file.

    docker-compose up

Debug as needed.  You can check the log files for any of the containers using `docker logs` to see what the problems might be.