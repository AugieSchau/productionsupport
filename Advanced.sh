#!/bin/bash

waitforkey() {
  echo "Press the Enter key to continue..." 
  read anykey
}

while :
do
clear
echo "******************************"       
echo "**** Select A Menu Option ****"       
echo "******************************"       
echo
echo "1 List files in the current directory"
echo "2 Change to another directory"        
echo "3 Create a new file"
echo "4 Delete a file"
echo "5 Edit a file"
echo "6 Move a file"
echo "7 Quit"
echo
echo "The current directory is :`pwd`"
echo
echo "Enter option:"
read option
echo

case $option in
  1)
    ls -l
    waitforkey
    ;;
  2)
    echo "Enter the directory to change to: "
    read newdir
    if [ -e $newdir -a -r $newdir ]
    then
      cd $newdir
      pwd
      waitforkey
    else
      echo "This directory is not accessible"
      echo "Check that it exists, or"
      echo "you have read permissions ..."
      waitforkey
    fi
    ;;
  3)
    echo "Enter the name of the file to be created: "
    read newfile
    touch $newfile
    ;;
  4)
    echo "Enter the name of the file(s) to delete: "
    read rmfile
    rm -i $rmfile
    ;;
  5)
    echo "Enter the name of the file(s) to edit: "
    read nanofile
    nano $nanofile
    ;;
  6)
    echo "Enter the name of the file(s) to move: "
    read mvfile
    echo "Enter the destination directory:"
    read mvdir
    mv $mvfile $mvdir
    ;;
  7)
    exit
    ;;
  *)
    echo "Enter a number from 1 to 7"
    waitforkey
    ;;
esac
done